package com.hb.todo;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.hb.todo.pojos.Todo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddTodoActivity extends AppCompatActivity {

    public static final String KEY_TODO_NAME = "todoName";
    public static final String KEY_TODO_URGENCY = "todoUrgency";
    public static final String KEY_TODO = "todo";

    // déclare les variables qui serviront à stocker les objet correspondant à chaque élément
    private Spinner spinner;
    private EditText edtTodo;
    private Button btnAdd;
    private Button btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_todo);

        // récupère les éléments
        spinner = findViewById(R.id.sprUrgency);
        btnAdd = findViewById(R.id.btnAdd);
        btnCancel = findViewById(R.id.btnCancel);
        edtTodo = findViewById(R.id.edtTodo);

        // converti un tableau en list
        final List<String> urgences = new ArrayList<>(Arrays.asList(
                new String[]{
                "Faiblement urgent",
                "Moyennement urgent",
                "Urgent"
        }));

        // créé un ArrayAdapter qui permet de faire le lien entre les données de la liste et le spinner
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, urgences);

        // on spécifie la vue qui permettra de mettre un style sur un élément du spinner
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

        // on plug le spinner avec l'adapter
        spinner.setAdapter(spinnerArrayAdapter);

        // gestion du clic sur btnAdd
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // on récupère le context de la'application
                Context context = getApplicationContext();

                // récupère le text saisie par l'utilisateur dans edtTodo
                String name = edtTodo.getText().toString();
                // récupère le text sélectionné dans le spinner
                String urgency = spinner.getSelectedItem().toString();

                if (name.length() > 3) {

                    // créé un intent pour renvoyer le résultat de cette activity à la main activity
                    Intent resultIntent = new Intent();

                    // version avec pojo
                    Todo todo = new Todo(name, urgency);
                    resultIntent.putExtra(KEY_TODO, todo);

                    // version sans pojo
                    // ajoute dans l'intent name et urgency
                    resultIntent.putExtra(KEY_TODO_NAME, name);
                    resultIntent.putExtra(KEY_TODO_URGENCY, urgency);

                    // indique que cette activity return OK
                    setResult(RESULT_OK, resultIntent);

                    finish();
                }
                else {
                    Toast toast = Toast.makeText(context, "Vous devez renseigner au moins 3 caractères", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });

        // gestion du clic sur btnCancel
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // créé un intent pour revenir sur la main activity
                // avec RESULT_CANCELED
                Intent resultIntent = new Intent();
                setResult(RESULT_CANCELED, resultIntent);
                // termine cette activity -> revient sur main activity
                finish();

            }
        });
    }
}
